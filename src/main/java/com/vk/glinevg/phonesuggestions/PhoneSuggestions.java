package com.vk.glinevg.phonesuggestions;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toList;

public class PhoneSuggestions {

    final static Map<Integer, List<Character>> PHONE_DIGIT_CHARACTERS = Map.of(
            0, emptyList(),
            1, emptyList(),
            2, asList('a', 'b', 'c'),
            3, asList('d', 'e', 'f', 'g'),
            4, asList('h', 'i', 'j'),
            5, asList('k', 'l', 'm'),
            6, asList('n', 'o', 'p'),
            7, asList('q', 'r', 's', 't'),
            8, asList('u', 'v', 'w'),
            9, asList('x', 'y', 'z')
    );

    public List<String> getAllSuggestions(int... digits) {

        Stream<List<Character>> characters = Arrays.stream(digits)
                .mapToObj(PHONE_DIGIT_CHARACTERS::get)
                .filter(not(Collection::isEmpty))
                .parallel();

        return characters
                .map(this::toStringBuilders)
                .reduce(this::suggestions)
                .orElseGet(Stream::empty)
                .map(StringBuilder::toString)
                .collect(toList());

    }

    private Stream<StringBuilder> suggestions(Stream<StringBuilder> source, Stream<StringBuilder> next) {
        List<StringBuilder> list = next.collect(toList());
        return source.flatMap(sb -> list.stream().map(n -> new StringBuilder(sb).append(n)));
    }

    private Stream<StringBuilder> toStringBuilders(List<Character> characters) {
        return characters.stream().map(ch -> new StringBuilder().append(ch));
    }

}