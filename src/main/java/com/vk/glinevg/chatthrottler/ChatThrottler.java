package com.vk.glinevg.chatthrottler;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.Objects.requireNonNull;

public class ChatThrottler {

    private final int messageLimit;
    private final long timeInterval;

    private final Clock clock;

    public ChatThrottler(int messageLimit, long timeInterval) {
        this(messageLimit, timeInterval, Clock.systemDefaultZone());
    }

    /**
     * Test only constructor
     */
    ChatThrottler(int messageLimit, long timeInterval, Clock clock) {
        this.messageLimit = messageLimit;
        this.timeInterval = timeInterval;
        this.clock = clock;
    }

    public boolean shouldThrottle(List<Message> messages) {
        requireNonNull(messages, "Messages cannot be null");
        if (messages.isEmpty()) {
            return true;
        }
        return messages.stream()
                .sorted(comparing(Message::getDispatchTime).reversed())
                .limit(messageLimit)
                .anyMatch(this::isMessageIntervalElapsed);
    }

    private boolean isMessageIntervalElapsed(Message message) {
        return message.getDispatchTime().plus(timeInterval, ChronoUnit.MILLIS).isBefore(LocalDateTime.now(clock));
    }

}
