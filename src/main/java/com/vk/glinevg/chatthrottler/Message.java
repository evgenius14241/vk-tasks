package com.vk.glinevg.chatthrottler;

import java.time.LocalDateTime;

public interface Message {
    LocalDateTime getDispatchTime();
}
