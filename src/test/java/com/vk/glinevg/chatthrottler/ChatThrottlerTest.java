package com.vk.glinevg.chatthrottler;

import com.vk.glinevg.chatthrottler.fixture.TestMessage;
import org.assertj.core.api.AbstractBooleanAssert;
import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.rangeClosed;
import static org.assertj.core.api.Assertions.assertThat;

public class ChatThrottlerTest {

    private Clock clock;

    @Before
    public void setUp() {
        clock = Clock.fixed(Instant.now().minusSeconds(60), ZoneId.systemDefault());
    }

    @Test
    public void sendIfMessagesCountPerIntervalIsNotExceeded() {
        List<Message> messages = givenMessagesDispatchedEveryMillisecond(50);
        assertCanSendMessage(15, 10, messages).isTrue();
    }

    @Test
    public void notSendIfMessagesCountPerIntervalExceeded() {
        List<Message> messages = givenMessagesDispatchedEveryMillisecond(50);
        assertCanSendMessage(5, 10, messages).isFalse();
    }

    @Test
    public void sendIfEmptyMessagesProvided() {
        assertCanSendMessage(1, 10, Collections.emptyList()).isTrue();
    }

    @Test(expected = NullPointerException.class)
    public void throwsNpeIfNullProvided() {
        assertCanSendMessage(1, 10, null);
    }

    private AbstractBooleanAssert<?> assertCanSendMessage(int limit, long timeInterval, List<Message> messages) {
        return assertThat(new ChatThrottler(limit, timeInterval, clock).shouldThrottle(messages));
    }

    private List<Message> givenMessagesDispatchedEveryMillisecond(int messagesCount) {
        List<Message> messages = rangeClosed(1, messagesCount)
                .mapToObj(i -> new TestMessage(LocalDateTime.now(clock).minus(i, ChronoUnit.MILLIS)))
                .unordered()
                .collect(toList());

        Collections.shuffle(messages);
        return messages;
    }

}