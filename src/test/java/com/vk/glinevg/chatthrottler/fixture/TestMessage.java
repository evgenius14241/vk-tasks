package com.vk.glinevg.chatthrottler.fixture;

import com.vk.glinevg.chatthrottler.Message;

import java.time.LocalDateTime;

public class TestMessage implements Message {

    private final LocalDateTime dispatchTime;

    public TestMessage(LocalDateTime dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    @Override
    public LocalDateTime getDispatchTime() {
        return dispatchTime;
    }
}
