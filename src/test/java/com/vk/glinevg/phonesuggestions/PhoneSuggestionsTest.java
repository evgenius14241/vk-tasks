package com.vk.glinevg.phonesuggestions;

import org.assertj.core.api.ListAssert;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PhoneSuggestionsTest {

    @Test
    public void getAllSuggestions() {
        assertSuggestions(2, 3).containsExactly(
                "ad", "ae", "af", "ag",
                "bd", "be", "bf", "bg",
                "cd", "ce", "cf", "cg"
        );
    }

    @Test
    public void emptyListIfDigitsMappedToEmptyCharacters() {
        assertSuggestions(0, 1, 1, 0).isEmpty();
    }

    @Test
    public void suggestsFirstNotEmptyCharacters() {
        assertSuggestions(0, 2).containsExactly("a", "b", "c");
    }

    @Test
    @Ignore
    public void performance() {
        long start = System.currentTimeMillis();
        new PhoneSuggestions().getAllSuggestions(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4);
        long elapsed = System.currentTimeMillis() - start;
    }

    private ListAssert<String> assertSuggestions(int... digits) {
        return assertThat(new PhoneSuggestions().getAllSuggestions(digits));
    }

}