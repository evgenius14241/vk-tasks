# vk-task

<style>
    .disable-pointer-events {
        pointer-events: none;
    }
</style>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/fomantic-ui/2.8.7/components/icon.min.css">
</head>

<body>

<div class="ui sidebar vertical left menu visible">
    <a class="item disable-pointer-events">
        <h3>Filters</h3>
    </a>
    <div class="ui form">
        <div class="field item">
            <label>Name</label>
            <label>
                <input type="text" placeholder="Contract Name">
            </label>
        </div>
    </div>

    <div class="ui form">
        <div class="field item">
            <label>Tags</label>
            <div class="ui fluid dropdown selection multiple <!--active&ndash;&gt; visible">
                <i class="dropdown icon"></i>
                <div class="default text">Enter tags</div>
                <div class="menu transition visible" style="display: none !important;">
                    <div class="item">Angular</div>
                    <div class="item selected">CSS</div>
                </div>
            </div>
        </div>
    </div>

    <div class="ui form">
        <div class="field item">
            <label>Created by</label>
            <label>
                <input type="text" placeholder="user@db.com">
            </label>
            <div class="ui checkbox">
                <input type="checkbox">
                <label>Me</label>
            </div>
        </div>
    </div>
    <div class="ui form">
        <div class="field item">
            <div class="ui checked checkbox">
                <input type="checkbox" checked="">
                <label>Enabled Contracts</label>
            </div>
        </div>
    </div>


</div>
<div class="ui container" style="padding-top: 1em">
    <div class="pusher">
        <h2 class="ui left floated header">Contracts</h2>
        <div class="ui clearing divider"></div>
        <div class="ui left floated pagination menu">
            <a class="icon item">
                <i class="left chevron icon"></i>
            </a>
            <a class="item">1</a>
            <a class="item">2</a>
            <a class="item">3</a>
            <a class="item">4</a>
            <a class="icon item">
                <i class="right chevron icon"></i>
            </a>
        </div>
        <div class="ui right floated small primary labeled icon button">
            <i class="plus icon"></i>Add Contract
        </div>
        <p></p>
        <div class="ui horizontal divider"></div>
        <div class="ui top attached tabular menu">
            <div class="center item">All
                <div class="ui blue label">42</div>
            </div>
            <div class="center item">Producer
                <div class="ui blue label">30</div>
            </div>
            <div class="center item">Consumer
                <div class="ui blue label">12</div>
            </div>
            <div class="center item">Pending Approval
                <div class="ui yellow label">5</div>
            </div>
            <div class="center item">Rejected
                <div class="ui red label">3</div>
            </div>
        </div>
        <div class="ui bottom attached active tab segment">
            <table class="ui single line table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Updated</th>
                    <th>Tags</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><h4>Pain 001</h4></td>
                    <td>September 14, 2013</td>
                    <td>
                        <a class="ui primary invert label">Tag</a>
                        <a class="ui primary label">Tag</a>
                        <a class="ui primary label">Tag</a>
                        <a class="ui primary label">Tag</a>

                    </td>
                    <td>
                        <button class="ui icon button">
                            <i class="edit icon"></i>
                        </button>
                        <button class="ui icon button">
                            <i class="trash icon"></i>
                        </button>
                    </td>
                </tr>
                </tbody>
                <!--<tfoot>
                <tr>
                    <th colspan="5">
                        <div class="ui left floated pagination menu">
                            <a class="icon item">
                                <i class="left chevron icon"></i>
                            </a>
                            <a class="item">1</a>
                            <a class="item">2</a>
                            <a class="item">3</a>
                            <a class="item">4</a>
                            <a class="icon item">
                                <i class="right chevron icon"></i>
                            </a>
                        </div>
                        <div class="ui right floated small primary labeled icon button">
                            <i class="plus icon"></i>Add Contract
                        </div>
                    </th>
                </tr>
                </tfoot>-->
            </table>
        </div>
    </div>
</div>

</body>
</html>

<style>
    .disable-pointer-events {
        pointer-events: none;
    }
</style>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/fomantic-ui/2.8.7/components/icon.min.css">

    <style type="text/css">

        .term-configuration-container {
            margin-top: 1.2em !important;
        }
    </style>

</head>
<body>

<div class="ui container" style="margin-top: 1em">
    <form class="ui form">
        <div class="field">
            <label>Name</label>
            <label>
                <input type="text" placeholder="Contract Name">
            </label>
        </div>
        <div class="field">
            <label>Tags</label>
            <div class="ui fluid dropdown selection multiple <!--active&ndash;&gt; visible">
                <i class="dropdown icon"></i>
                <div class="default text">Enter tags</div>
                <div class="menu transition visible" style="display: none !important;">
                    <div class="item">Angular</div>
                    <div class="item selected">CSS</div>
                </div>
            </div>
        </div>
        <div class="field">
            <label>
                <div class="ui slider checkbox">
                    <input type="checkbox" name="newsletter" checked="">
                    <label>Consumer</label>
                </div>
            </label>
            <div class="field">
                <input type="text" placeholder="Condition">
            </div>
        </div>
        <div class="field">
            <label>
                <div class="ui slider checkbox">
                    <input type="checkbox" name="newsletter" checked="">
                    <label>Producer</label>
                </div>
            </label>
            <div class="fields">
                <div class="four wide field">
                    <div class="ui fluid dropdown selection multiple <!--active&ndash;&gt; visible">
                        <i class="dropdown icon"></i>
                        <div class="default text">Select Application</div>
                        <div class="menu transition visible" style="display: none !important;">
                            <div class="item">Angular</div>
                            <div class="item selected">CSS</div>
                        </div>
                    </div>
                </div>
                <div class="twelve wide field">
                    <input type="text" placeholder="Condition">
                </div>
            </div>
        </div>
        <div class="ui segment">
            <div class="field">
                <label>
                    Terms
                </label>
                <div class="ui relaxed divided list">
                    <div class="item">
                        <div class="content">
                            <a class="header">Validated by XSD
                                <label class="right floated ui label">XSD Validation</label>
                            </a>
                            <div class="description">
                                <div class="inline fields term-configuration-container">
                                    <label>File</label>
                                    <a>my-super-awesome-xsd.xsd</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="content">
                            <a class="header">Checksum tag should present
                                <label class="right floated ui label">Regex Contains</label>
                            </a>
                            <div class="description">
                                <div class="field term-configuration-container">
                                    <label>Pattern</label>
                                    <input type="text" placeholder="Regular Expression">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ui primary button">
                        <i class="ui plus icon"></i>Add Term
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>

